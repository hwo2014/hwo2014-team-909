(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:require [clojure.core.async :as async])
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]])
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def message-channel (async/chan 10))
(def throttle-channel (async/chan))
(def latest-throttle 0.5)

(defmulti handle-msg :msgType)

(defn most-recent-throttle []
  (let [[throttle _] (async/alts!! [throttle-channel] :default latest-throttle)]
    (def latest-throttle throttle)
    throttle))

(defmethod handle-msg "carPositions" [msg channel]
  (async/>!! message-channel (first (filter #(= ((% :id) :name) "booyan") (msg :data))))
  (async/<!! message-channel) ; ignore the above until we have someone who's interested in it
  (let [response-throttle (most-recent-throttle)]
    (send-message channel {:msgType "throttle" :data response-throttle})))

(defmethod handle-msg :default [msg channel]
  (send-message channel {:msgType "ping" :data "ping"}))

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameEnd" (println "Race ended")
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (log-msg msg)
    (handle-msg msg channel)
    (recur channel)))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))
