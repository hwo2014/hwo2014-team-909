(ns hwo2014bot.kinematics)

; The maximum speed allowable in a bend is limited by the cetripetal force.
; At a given speed, the centripetal specific force is given by:
;
; F = mv^2 / r
;
; For simplicity, we assume the mass is one, which gives us the maximum speed as:
;
; F_max = v^2 / r
; v^2 = F_max * r
; v = sqrt(F_max * r)
;
; Friction is proportional to mass, so this equates to:
;
; s = sqrt(mu * r)
(defn maximum-curve-speed [radius lateral-friction]
  (Math/sqrt (* lateral-friction (Math/abs radius)))) 

; The true radius of a curve, which depends on the lane number, the
; direction of the turn, and the distance from center of the lane.
; A negative radius is taken to indicate that the direction of the turn
; is the opposite that indicated by the angle.
(defn true-radius [segment lane]
  (let [radius (segment :radius)
        distance (lane :distance-from-center)
        final-val ((if (< (segment :angle) 0) + -) radius distance)]
    (if (= 0 radius)
      Double/NaN
      (if (or (and (< radius 0) (> final-val 0))
              (and (> radius 0) (< final-val 0)))
        Double/NaN
        final-val))))


; The maximum speed allowable in a segment. If the segment is straight, the
; maximum speed is infinite and this function returns :inf. Otherwise, it returns
; the maximum curve speed.
(defn maximum-speed [segment lane]
  (if (segment :straight)
    :inf
    (maximum-curve-speed (true-radius segment lane) (segment :lateral-friction))))

; The throttle required for constant speed with a given longitudinal friction.
; We assume that the longitudinal friction is a constant force opposing motion.
; We then have:
;
; F_t = F_f
; F_t = mu
;
; This function assumes that friction is normalized to match throttle,
; i.e. throttle of 1 corresponds to force of 1.
(defn constant-speed-throttle [longitudinal-friction]
  (longitudinal-friction))

; The distance required to get to a given speed at zero throttle. Per standard
; dynamics:
;
; F = ma
; F = a (m = 1 per our standard assumption)
; dv = at
; t = v/a
;
; where t is the time to decelerate to that speed. We can then compute the distance:
;
; s = v_0t + at^2
(defn coasting-distance [starting-speed target-speed longitudinal-friction]
  (let [speed-difference (- target-speed starting-speed)
        coasting-time (/ speed-difference longitudinal-friction)]
    (+ (* starting-speed coasting-time)
       (* longitudinal-friction coasting-time coasting-time))))

; The maximum entrance speed into a segment in order to exit the segment
; at a given speed, assuming zero throttle. This is a dual function
; to the preceding one, which gives the coasting distance to achieve
; a speed. Again, using standard kinematics:
;
; v_0^2 = v_f^2 - 2as
; v_0 = sqrt(v_f^2 - 2as)
(defn initial-coasting-speed [target-speed distance longitudinal-friction]
  (Math/sqrt (- (* target-speed target-speed) (* 2 distance longitudinal-friction))))
