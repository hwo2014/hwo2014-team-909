(ns hwo2014bot.control
  (:require [hwo2014bot.kinematics :as k]))

; PRIMITIVES
;
; There is a lot of basic manipulation needed to properly compute the optimal path.
; These methods carry them out. If enough of them are put together, they may
; be taken out into their own namespace.

; Rotates a sequence by the passed number. In other words:
;
; (rotate 3 [a b c d]) => [c d a b]
(defn rotate-seq [rotation-number lst]
  (let [list-length (count lst)]
    (if (<= list-length <= 1)
      lst
      (let [actual-rotation-number (mod rotation-number list-length)]
        (concat (drop actual-rotation-number lst)
                (take actual-rotation-number lst))))))

; The custom minimum basically handles cases where a or b are :inf appropriately, so that
; we can return infinite minimum speeds.
(defn custom-min [a b]
  (if (= :inf a)
    b
    (if (= :inf b)
      a
      (min a b))))

; Computes the maximum speed at which a vehicle can enter a segment.
;
; There are two limiting factors:
; - The radius of the segment
; - The maximum exit speed.
;
; The maximum exit speed is determined by the segments following it; consequenly
; the proper way to use this method is by backtracking from the end.
(defn maximum-segment-entrance-speed [maximum-exit-speed segment]
  (let [local-max (k/maximum-speed segment 0)]
    (if (or (= :inf local-max) (= :inf maximum-exit-speed) (< local-max maximum-exit-speed))
      local-max
      (let [max-initial-speed (k/initial-coasting-speed maximum-exit-speed
                                                        (segment :length)
                                                        (segment :longitudinal-friction))]
        (if (< max-initial-speed local-max)
          max-initial-speed
          local-max)))))
      

; Computes the maximum speed at which a vehicle can enter the passed track.
;
; This method must recurse through every segment of the track, in reverse order,
; to properly handle the case where a fast segment is follows by a slower segment
; This happens when a straight segment is followed by a curved segment, or a
; curved segment by another curved segment with a tigher radius.
(defn maximum-track-entrance-speed [track]
  (reduce maximum-segment-entrance-speed :inf (reverse track)))

; Computes the maximum exit speed at the end of a segment. This speed is computed based on
; the curvature of the current segment and the maximum exit speed of the next segment.
;
; If the track is completely straight (unlikely, but possible), the result will be :inf.
(defn maximum-track-exit-speed [track]
  (maximum-track-entrance-speed (rotate-seq 1 track)))

; Computes the throttle setting for the current segment index and in-segment position
; in the passed track configuration.
;
; This method does not update the state of the segments; in particular, it does not
; update the friction values.
;
; Parameters:
; segment  the index of the current segment.
; position the position along the current segment. This is an angle if the segment is a
;          curve, and a length if it's a straight line.
; lane     the lane of the car
; track    the track configuration, consisting of a sequence of curves and straight lines.
(defn throttle [segment position lane track]
  ; We first need to know the maximum exit speed.
  )

