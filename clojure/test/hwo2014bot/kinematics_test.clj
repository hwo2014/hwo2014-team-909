(ns hwo2014bot.kinematics-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.kinematics :refer :all]))

(defn is-float-equal [a b]
  (is (> 0.001 (Math/abs (- a b)))))

(deftest maximum-curve-speed-test 
  (testing "Correct speed for sensible input."
    (is-float-equal (maximum-curve-speed 4 1) 2))
  (testing "Zero lateral friction implies inability to turn."
    (is-float-equal (maximum-curve-speed 4 0) 0))
  (testing "Negative radii are treated as positive radii."
    (is (= (maximum-curve-speed 4 1) (maximum-curve-speed -4 1))))
  (testing "Negative lateral friction is nonsensical."
    (is (Double/isNaN (maximum-curve-speed 4 -1)))))

(deftest true-radius-test
  (testing "Correct radius for positive distance from center, right turn."
    (is-float-equal (true-radius {:angle 20 :radius 100} 
                                  {:distance-from-center 20}) 80))
  (testing "Correct radius for positive distance from center, left turn."
    (is-float-equal (true-radius {:angle -33.2 :radius 97.3}
                                  {:distance-from-center 12.1}) 109.4))
  (testing "Correct radius for negative distance from center, right turn."
    (is-float-equal (true-radius {:angle 20 :radius 100}
                                 {:distance-from-center -20}) 120))
  (testing "Correct radius for negative distance from center, left turn."
    (is-float-equal (true-radius {:angle -33.2 :radius 97.3}
                                  {:distance-from-center -12.1}) 85.2))
  (testing "If radius is negative, reverse the logic."
    (is-float-equal (true-radius {:angle 20 :radius -100} 
                                  {:distance-from-center 20}) -120))
  (testing "If radius is zero, the result is not a number."
    (is (Double/isNaN (true-radius {:angle 20 :radius 0}
                                   {:distance-from-center 20}))))
  (testing "If radius is too small, the result is not a number."
    (is (Double/isNaN (true-radius {:angle 20 :radius 10}
                                   {:distance-from-center 20})))
    (is (Double/isNaN (true-radius {:angle 20 :radius 10}
                                   {:distance-from-center -20})))))
